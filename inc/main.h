
#define PAGE_SAMPLES 28
#define CHEW_MARK    5

typedef struct{
  int16_t xBuf[PAGE_SAMPLES];
  int16_t yBuf[PAGE_SAMPLES];
  int16_t zBuf[PAGE_SAMPLES];
  uint8_t sessionNum;
  uint8_t chewing[PAGE_SAMPLES/CHEW_MARK];
} readStruct_t;

typedef union{
  readStruct_t readStruct;
  uint8_t writeBuf[0x100];
} write2Mem_t;
