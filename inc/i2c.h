
void i2cMC3635Init(void);

//read data from accelerometer
void i2cAccDataRead(int16_t *x, int16_t *y, int16_t *z, uint8_t samples);

/***********************************************************************
check FIFO interrupt flag
***********************************************************************/
bool i2cGetFIFOintFlag(void);