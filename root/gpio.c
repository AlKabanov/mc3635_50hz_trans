
#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include "gpio.h"

#define LED_PORT      gpioPortC           
#define LED_BIT       2				
#define LED_MASK      (1 << LED_BIT)

#define PIN_SPI_CS                3			//CSN = PD3
#define PORT_SPI_CS               gpioPortD

#define PIN_SPI_MEM_CS          8
#define PORT_SPI_MEM_CS         gpioPortD

#define PIN_SPI_X_CS          13
#define PORT_SPI_X_CS         gpioPortB

#define RST_PORT gpioPortC
#define RST_PIN  13	
#define LoRaRST_EN      GPIO_PinModeSet(RST_PORT, RST_BIT, gpioModePushPull, 0)
#define LoRaRST_ON      GPIO_PinOutSet(RST_PORT, RST_BIT)
#define LoRaRST_OFF     GPIO_PinOutClear(RST_PORT, RST_BIT)

#define TEMP_PWR_PORT gpioPortA
#define TEMP_PWR_BIT  0
#define TEMP_PWR_EN   GPIO_PinModeSet(TEMP_PWR_PORT, TEMP_PWR_BIT, gpioModePushPull, 0)
#define TEMP_PWR_ON   GPIO_PinOutSet(TEMP_PWR_PORT, TEMP_PWR_BIT)
#define TEMP_PWR_OFF  GPIO_PinOutClear(TEMP_PWR_PORT, TEMP_PWR_BIT)

#define PULS_PWR_PORT gpioPortC
#define PULS_PWR_BIT  0
#define PULS_PWR_EN   GPIO_PinModeSet(PULS_PWR_PORT, PULS_PWR_BIT, gpioModePushPull, 0);\
                      GPIO_DriveModeSet(PULS_PWR_PORT,gpioDriveModeHigh)

#define PULS_PWR_ON   GPIO_PinOutSet(PULS_PWR_PORT, PULS_PWR_BIT)
#define PULS_PWR_OFF  GPIO_PinOutClear(PULS_PWR_PORT, PULS_PWR_BIT)

#define LED_EN  GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModePushPull, 0)
#define LED_DIS GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModeDisabled, 0)
#define LED_ON  GPIO_PinOutSet(LED_PORT, LED_BIT)
#define LED_OFF GPIO_PinOutClear(LED_PORT, LED_BIT)
#define LED_TOGGLE GPIO_PinOutToggle(LED_PORT, LED_BIT)

#define LDO_PORT      gpioPortF           
#define LDO_BIT       2		
#define LDO_EN  GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModePushPull, 0)
#define LDO_DIS GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModeDisabled, 0)
#define LDO_ON  GPIO_PinOutSet(LDO_PORT, LDO_BIT)
#define LDO_OFF GPIO_PinOutClear(LDO_PORT, LDO_BIT)
                        
#define GNSSPWR_PORT gpioPortC
#define GNSSPWR_PIN 0
#define GNSSPWR_EN  GPIO_PinModeSet(GNSSPWR_PORT, GNSSPWR_PIN, gpioModePushPull, 0)
#define GNSSPWR_DIS GPIO_PinModeSet(GNSSPWR_PORT, GNSSPWR_PIN, gpioModeDisabled, 0)
#define GNSSPWR_ON  GPIO_PinOutSet(GNSSPWR_PORT, GNSSPWR_PIN)
#define GNSSPWR_OFF GPIO_PinOutClear(GNSSPWR_PORT, GNSSPWR_PIN)




/** Array of GPIO IRQ hooks for system/user/odd/even IRQs */
GPIO_hook_t GPIO_Hooks[hookSize];

/**************************************************************************//**
 * @brief Setup GPIO interrupt to set the time
 *****************************************************************************/
void gpioSetup(void)
{
  /* Enable GPIO clock */
  CMU_ClockEnable(cmuClock_GPIO, true);
  
  GPIO_PinModeSet(PORT_SPI_CS, PIN_SPI_CS, gpioModePushPull, 1); // SX1276
  
  GPIO_PinModeSet(PORT_SPI_MEM_CS, PIN_SPI_MEM_CS, gpioModePushPull, 1); // memory
  
  GPIO_PinModeSet(PORT_SPI_X_CS, PIN_SPI_X_CS, gpioModePushPull, 1); // memory
  
 //  Configure PC14 as input 
  GPIO_PinModeSet(RF_DIO0_PORT, RF_DIO0_PIN, gpioModeInput, 1);
  //  Configure PC14 as input 
  GPIO_PinModeSet(X_INT_PORT, X_INT_PIN, gpioModeInput, 1);

  // Set rasing edge interrupt 
  GPIO_IntConfig(RF_DIO0_PORT, RF_DIO0_PIN, true, false, true);
  // Set rasing edge interrupt 
  GPIO_IntConfig(X_INT_PORT, X_INT_PIN, true, false, true);
  
  NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  
  
/*
  // Configure PB9 as input 
  GPIO_PinModeSet(gpioPortB, 9, gpioModeInput, 0);
  

  // Set falling edge interrupt 
  GPIO_IntConfig(gpioPortB, 13, false, true, true);
  NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
*/
  LED_EN;
  LDO_EN;
  GNSSPWR_EN;
 //TEMP_PWR_EN;
 // PULS_PWR_EN;
}

void gpioSetLED(void)
{
  LED_ON;
}

void gpioClearLED(void)
{
  LED_OFF;
}

void gpioToggleLED(void)
{
  LED_TOGGLE;
}
void gpioLDOOn(void)
{
  LDO_ON;
}

void gpioLDOOff(void)
{
  LDO_OFF;
}

void gpioTEMPPWRON(void)
{
  TEMP_PWR_ON;
}

void gpioTEMPPWROFF(void)
{
  TEMP_PWR_OFF;
}
void gpioPULSEPWRON(void)
{
  PULS_PWR_ON;
}

void gpioPULSEPWROFF(void)
{
  PULS_PWR_OFF;
}


void gpioGNSSPWROn(void)
{
  GNSSPWR_ON;
}

void gpioGNSSPWROff(void)
{
  GNSSPWR_OFF;
}

// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val)
{
  if(val == 0 || val == 1)
  { // drive pin
       GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModePushPull, 0);
       if (val)GPIO_PinOutSet(RST_PORT, RST_PIN);
       else    GPIO_PinOutClear(RST_PORT, RST_PIN);
  } else 
    { // keep pin floating
        GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModeDisabled, 1);
    }
}

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val)
{
	//not used 
}

// set radio NSS pin to given value
void gpioNSS (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Enable (Active Low)
		
}

// set memory NSS pin to given value
void gpioNSS_MEM (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_MEM_CS, PIN_SPI_MEM_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_MEM_CS, PIN_SPI_MEM_CS);	//  SPI Enable (Active Low)
		
}

// set axcelerometer NSS pin to given value
void gpioNSS_X (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_X_CS, PIN_SPI_X_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_X_CS, PIN_SPI_X_CS);	//  SPI Enable (Active Low)
		
}


/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB9)
 *        Sets the hours
 *****************************************************************************/

bool sleepFlag = false;
/*
void GPIO_ODD_IRQHandler(void)
{
  // Acknowledge interrupt 
  GPIO_IntClear(1 << 13); 
  sleepFlag = true;
}
*/
bool gpioGetSleepFlag(void)
{
  bool temp = sleepFlag;
  sleepFlag = false;
  return temp;
}



/**************************************************************************//**
 * @brief GPIO Interrupt handler (PC14)
 *        Sets the minutes
 *****************************************************************************/
#define GPIO_EVEN_MASK	0x55555555						///< Mask for even interrupts



void GPIO_EVEN_IRQHandler(void)
{  
  uint32_t mask;
  // Get interrupt bits
  mask = GPIO_IntGet() & GPIO_EVEN_MASK; 
  // Acknowledge interrupt 
  GPIO_IntClear(mask);
  for(int i = 0; i < hookSize; i++)
  {
    if(GPIO_Hooks[i].callback && (mask & GPIO_Hooks[i].mask))
      GPIO_Hooks[i].callback();
  }
  
}