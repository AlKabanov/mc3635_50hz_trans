/***************************************************************************//**
 * @file
 * @brief I2C AFE4404 pulse meter
 * @author Kabanov
 * @version 1.0.0
  ******************************************************************************/

#include <em_i2c.h>
#include <em_cmu.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "gpio.h"
#include "i2c.h"

#define SDA_PORT gpioPortB
#define SDA_BIT  11
#define SCL_PORT gpioPortB
#define SCL_BIT  12

#define I2Cx I2C1

#define MC3635_ADDR (0x6C << 1)


#define MC3635_REG_EXT_STAT_1       (0x00)
#define MC3635_REG_EXT_STAT_2       (0x01)
#define MC3635_REG_XOUT_LSB         (0x02)
#define MC3635_REG_XOUT_MSB         (0x03)
#define MC3635_REG_YOUT_LSB         (0x04)
#define MC3635_REG_YOUT_MSB         (0x05)
#define MC3635_REG_ZOUT_LSB         (0x06)
#define MC3635_REG_ZOUT_MSB         (0x07)
#define MC3635_REG_STATUS_1         (0x08)
#define MC3635_REG_STATUS_2         (0x09)
#define MC3635_REG_MODE_C           (0x10)
#define MC3635_REG_WAKE_C           (0x11)
#define MC3635_REG_SNIFF_C          (0x12)
#define MC3635_REG_SNIFFTH_C        (0x13)
#define MC3635_REG_IO_C             (0x14)
#define MC3635_REG_RANGE_C          (0x15)

#define MC3635_REG_FIFO_C           (0x16)
#define MC3635_REG_INTR_C           (0x17)
#define MC3635_REG_PROD             (0x18)
#define MC3635_REG_POWER_MODE       (0x1C)
#define MC3635_REG_DMX              (0x20)
#define MC3635_REG_DMY              (0x21)

#define MC3635_REG_DMZ              (0x22)
#define MC3635_REG_RESET            (0x24)
#define MC3635_REG_XOFFL            (0x2A)
#define MC3635_REG_XOFFH            (0x2B)
#define MC3635_REG_YOFFL            (0x2C)
#define MC3635_REG_YOFFH            (0x2D)
#define MC3635_REG_ZOFFL            (0x2E)
#define MC3635_REG_ZOFFH            (0x2F)
#define MC3635_REG_XGAIN            (0x30)
#define MC3635_REG_YGAIN            (0x31)
#define MC3635_REG_ZGAIN            (0x32)
#define MC3635_REG_OPT              (0x3B)
#define MC3635_REG_LOC_X            (0x3C)
#define MC3635_REG_LOC_Y            (0x3D)
#define MC3635_REG_LOT_dAOFSZ       (0x3E)
#define MC3635_REG_WAF_LOT          (0x3F)

typedef enum
{
    RANGE_2G   = 0,
    RANGE_4G   = 1,
    RANGE_8G   = 2,
    RANGE_16G  = 3,
    RANGE_12G  = 4,	
}   MC3635_range_t;

typedef enum
{
    RESOLUTION_6BIT    = 0, 
    RESOLUTION_7BIT    = 1, 
    RESOLUTION_8BIT    = 2, 
    RESOLUTION_10BIT   = 3, 
    RESOLUTION_12BIT   = 4, 
    RESOLUTION_14BIT   = 5,  //(Do not select if FIFO enabled)
}   MC3635_resolution_t;


typedef enum
{
  DEFAULT = 0,
  LP_14_PRE_14                  = 0x05,
  ULP_25_LP_28_PRE_28           = 0x06,
  ULP_50_LP54_PRE_55            = 0x07,
  ULP_100_LP105_PRE_80          = 0x08,
  ULP_190_LP_210                = 0x09,
  ULP_380_LP_400                = 0x0A,
  ULP_750_LP_600                = 0x0B,
  ULP_1100                      = 0x0C,
  ULP_1300                      = 0x0F,
}   MC3635_cwake_ODR_t;

    
typedef enum
{
  SLEEP      = 0x0,
  STANDBY    = 0x01,
  SNIFF      = 0x02,
  CWAKE      = 0x05, 
  TRIG       = 0x07,  
} MC3635_mode_t;

typedef enum 
{
  LOW_POWER =0,
  ULOW_POWER = 3,
  PRE_POWER = 4,
}MC3635_power_t;


static int writeReg(uint8_t regAddr, uint8_t data);
static uint8_t readReg(uint8_t regAddr);
static void reset(void);
static void SetWakePowerMode(MC3635_power_t pm);
static void SetSniffPowerMode(MC3635_power_t pm);
static void SetCWakeSampleRate(MC3635_cwake_ODR_t sample_rate);
static void SetResolutionCtrl(MC3635_resolution_t resolution);
static void SetRangeCtrl(MC3635_range_t range);


const I2C_Init_TypeDef g_i2cInit = I2C_INIT_DEFAULT;

uint8_t readVal = 0;
int16_t offsetX,offsetY,offsetZ;
static bool FIFOIntFlag = false;

/************************************************************************
interrupt callback functions
************************************************************************/
static void i2cFIFOThresholdInterrupt(void)
{
  FIFOIntFlag = true;
  readVal = readReg( MC3635_REG_STATUS_2);  //clear interrupt
}
/***********************************************************************
check FIFO interrupt flag
***********************************************************************/
bool i2cGetFIFOintFlag(void)
{
  return FIFOIntFlag;
}

//Set the operation mode  
void SetMode(MC3635_mode_t mode)
{
    uint8_t value;
    value = readReg(MC3635_REG_MODE_C);
    value &= 0xf0;
    value |= mode;
    writeReg(MC3635_REG_MODE_C, value);
}



void i2cMC3635Init(void)
{
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_I2C1, true);
    
    GPIO_PinModeSet(SDA_PORT, SDA_BIT, gpioModeWiredAnd, 1);
    GPIO_PinModeSet(SCL_PORT, SCL_BIT, gpioModeWiredAnd, 1);
    
    for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
    
    /* Enable pins at location 1*/
    I2Cx->ROUTE = I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN | (1 << _I2C_ROUTE_LOCATION_SHIFT);
                  
    I2C_Init(I2Cx, &g_i2cInit);
    
    
    reset();
     for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
     writeReg(0x1b,0xff);
     SetMode(STANDBY);
     writeReg(0x1b,0xff);
     
     SetWakePowerMode(PRE_POWER);
     SetSniffPowerMode(PRE_POWER);
     SetRangeCtrl(RANGE_2G);
     SetResolutionCtrl(RESOLUTION_12BIT);
     //SetCWakeSampleRate(LP_14_PRE_14);
     SetCWakeSampleRate(ULP_25_LP_28_PRE_28);
     //SetCWakeSampleRate(ULP_50_LP54_PRE_55); ULP_25_LP_28_PRE_28 
     writeReg(MC3635_REG_FIFO_C,0x80);  //fifo reset
     readVal = readReg( MC3635_REG_STATUS_2);  //clear interrupt
     __no_operation();
     for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
     writeReg(MC3635_REG_FIFO_C,0x20| 0x40 | 28);  //watermark, fifo enable, threshold = 28
     writeReg(MC3635_REG_INTR_C,0x03 | 0x40);  // Int push-pull active high, fifo threshold interrupt enable
     
     // set callback fo interrupt from DIO0  
     GPIO_SetCallback(mc3635INT, i2cFIFOThresholdInterrupt, X_INT_MASK);
     SetMode(CWAKE);
     
     readVal = readReg(MC3635_REG_FIFO_C);
     __no_operation();
     readVal = readReg(MC3635_REG_INTR_C);
     __no_operation();
    
    //offsetX = readReg(MC3635_REG_XOFFL);
    //offsetX +=(((int16_t)readReg(MC3635_REG_XOFFH))<<8)&0x7FFF ;
    //if(offsetX & 0x4000)offsetX |= 0x8000U; 
}


//write bytes register after initialization
static int writeReg(uint8_t regAddr, uint8_t data)
{
  uint8_t dataToSend[2];
  
  I2C_TransferSeq_TypeDef i2cFrame;
	
  i2cFrame.addr = MC3635_ADDR;
  int retCode;
  
  dataToSend[0] = regAddr;
  dataToSend[1] = data;
  
  i2cFrame.flags = I2C_FLAG_WRITE;
  i2cFrame.buf[0].data = dataToSend;
  i2cFrame.buf[0].len = 2;
  
  //initiate transfer
  I2C_TransferInit(I2Cx, &i2cFrame);
  //use polled transfer
  while ((retCode = I2C_Transfer(I2Cx)) == i2cTransferInProgress);
  
  return(retCode);
  
}
//read 6 bytes data reg
int16_t lastX = 0, lastY=0, lastZ=0;
void i2cAccDataRead(int16_t *x, int16_t *y, int16_t *z, uint8_t samples)
{
  I2C_TransferSeq_TypeDef i2cFrame;
  uint8_t regToRead[100];
  uint8_t regAddr = MC3635_REG_XOUT_LSB;
  FIFOIntFlag = false;
	
  i2cFrame.addr = MC3635_ADDR;
	
  //int retCode;
for(int i = 0; i < samples; i++)
{  
  i2cFrame.flags = I2C_FLAG_WRITE_READ;
     
	
  i2cFrame.buf[0].data = &regAddr;
  i2cFrame.buf[0].len = 1;

          
  i2cFrame.buf[1].data = regToRead;
  i2cFrame.buf[1].len = 6;
          
          //initiate transfer
  I2C_TransferInit(I2Cx, &i2cFrame);
          
          //use polled transfer
  while (I2C_Transfer(I2Cx) == i2cTransferInProgress);
  
  
    x[i] = regToRead[0];
    x[i] +=((int16_t)regToRead[1])<<8;
    
    y[i] = regToRead[2];
    y[i] +=((int16_t)regToRead[3])<<8;
    
    z[i] = regToRead[4];
    z[i] +=((int16_t)regToRead[5])<<8;
    if((x[i] > 4000)||(x[i]<-4000))x[i] = lastX;
    else lastX = x[i];
    if((y[i] > 4000)||(y[i]<-4000))y[i] = lastY;
    else lastY = y[i];
    if((z[i] > 4000)||(z[i]<-4000))z[i] = lastZ;
    else lastZ = z[i];
}
readVal = readReg( MC3635_REG_STATUS_2);  //clear interrupt
  
}

//read 1 byte register after initialization

static uint8_t readReg(uint8_t regAddr)
{
  I2C_TransferSeq_TypeDef i2cFrame;
  uint8_t regToRead[1];
  uint8_t regRead;
	
  i2cFrame.addr = MC3635_ADDR;
	
  //int retCode;
  
  i2cFrame.flags = I2C_FLAG_WRITE_READ;
     
	
  i2cFrame.buf[0].data = &regAddr;
  i2cFrame.buf[0].len = 1;

          
  i2cFrame.buf[1].data = regToRead;
  i2cFrame.buf[1].len = 1;
          
          //initiate transfer
  I2C_TransferInit(I2Cx, &i2cFrame);
          
          //use polled transfer
  while (I2C_Transfer(I2Cx) == i2cTransferInProgress);
  
  regRead = regToRead[0];
  //if(regRead & 0x800000 )reg
    
  return(regRead);
   
}

static void reset(void)
{
  writeReg(MC3635_REG_MODE_C, 0x01);  //standby mode
  writeReg(MC3635_REG_RESET, 0x40);
  for(int i = (4655/10)*1; i> 0;i--); //delay 1mS 
  writeReg(0x0D, 0x40);      //I2C interface
  writeReg(0x0f, 0x42);      //initialization
  writeReg(0x20, 0x01);      //initialization
  writeReg(0x21, 0x80);      //initialization
  writeReg(0x28, 0x00);      //initialization
  writeReg(0x1A, 0x00);      //initialization
  
}

static void SetWakePowerMode(MC3635_power_t pm)
{
     uint8_t value;
     SetMode(STANDBY);
     value = readReg(MC3635_REG_POWER_MODE);
     value &= 0xf8;
     value |= pm;
     writeReg(MC3635_REG_POWER_MODE, value);
}

static void SetSniffPowerMode(MC3635_power_t pm)
{
     uint8_t value;
     SetMode(STANDBY);
     value = readReg(MC3635_REG_POWER_MODE);
     value &= 0x8f;
     value |= pm<<4;
     writeReg(MC3635_REG_POWER_MODE, value);
}

static void SetCWakeSampleRate(MC3635_cwake_ODR_t sample_rate) 
{   
     SetMode(STANDBY);
     writeReg(MC3635_REG_WAKE_C, sample_rate);
}
//Set the resolution control
static void SetResolutionCtrl(MC3635_resolution_t resolution)
{
     uint8_t value;
     SetMode(STANDBY);
     value = readReg(MC3635_REG_RANGE_C);
     value &= 0x70;
     value |= resolution;
     writeReg(MC3635_REG_RANGE_C, value);
}
//Set the range control
static void SetRangeCtrl(MC3635_range_t range)
{
    uint8_t value;    
    SetMode(STANDBY);
    value = readReg(MC3635_REG_RANGE_C);
    value &= 0x8f;
    value |= range << 4;
    writeReg(MC3635_REG_RANGE_C, value);

}