#include <em_chip.h>
#include <em_emu.h>
#include <em_cmu.h>
#include <em_adc.h>
#include "adc.h"

void adcInit(void)
{
/* Base the ADC configuration on the default setup. */
  ADC_Init_TypeDef init  = ADC_INIT_DEFAULT;
  
  init.timebase = ADC_TimebaseCalc(0);
  init.prescale = ADC_PrescaleCalc(400000, 0);  //adc clock = 400kHz
  CMU_ClockEnable(cmuClock_ADC0, true);
  ADC_Init(ADC0, &init);
}

uint8_t adcRumRead(void)
{
  ADC_InitSingle_TypeDef single_init = ADC_INITSINGLE_DEFAULT;
  
  uint32_t out = 0;
  
  
  single_init.reference = adcRef1V25;     //reference 1.25V
  single_init.resolution = adcRes8Bit;   // eight bit resolution for SFMonitor
  single_init.input   	= adcSingleInpCh5;
  ADC_InitSingle(ADC0, &single_init);
  
  
 //ommit first measurement
  
  // Start one ADC sample
  ADC_Start(ADC0, adcStartSingle);
  // Active wait for ADC to complete
  while ((ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0) ; 
//  for(int i = 0; i < 3; i++)
//  {
//     // Start one ADC sample
//    ADC_Start(ADC0, adcStartSingle);
//    // Active wait for ADC to complete
//    while ((ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0) ; 
//    out += ADC_DataSingleGet(ADC0)&0xFFU; //8 bits resolution
//  }
//   
  out = ADC_DataSingleGet(ADC0)&0xFFU; //8 bits resolution;
  //CMU_ClockEnable(cmuClock_ADC0, false);
  return out;
}


int32_t adcTempRead(void)
{
   ADC_InitSingle_TypeDef single_init = ADC_INITSINGLE_DEFAULT;
  
  uint32_t result = 0;
  int32_t out = 0;
  
  /* Reference must be VCC */
  single_init.reference = adcRefVDD;
  single_init.resolution = adcRes12Bit;
  single_init.input   	= adcSingleInpCh7;
  ADC_InitSingle(ADC0, &single_init);
  
  
 //ommit first measurement
  
  // Start one ADC sample
  ADC_Start(ADC0, adcStartSingle);
  // Active wait for ADC to complete
  while ((ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0) ; 
  for(int i = 0; i < 10; i++)
  {
     // Start one ADC sample
    ADC_Start(ADC0, adcStartSingle);
    // Active wait for ADC to complete
    while ((ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0) ; 
    result += ADC_DataSingleGet(ADC0)&0xFFFU; //12 bits resolution
  }
   
  result /= 10;
  out = ((2862 - result)*1000)/4078;
  
  //CMU_ClockEnable(cmuClock_ADC0, false);
  return out;
}